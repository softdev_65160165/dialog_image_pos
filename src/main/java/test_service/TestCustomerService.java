/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author adisa
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer c : cs.getCustomers()){
            System.out.println(c);
        }
        
        System.out.println(cs.getByTel("0888868888"));
        Customer cus1 = new Customer("kob", "0999999999");
        cs.addNew(cus1);
        for(Customer c: cs.getCustomers()){
            System.out.println(c);
        }
        
        Customer delCus = cs.getByTel("0999999999");
        delCus.setTel("0999999991");
        cs.update(delCus);
        System.out.println("After Updated");
        for(Customer c: cs.getCustomers()){
            System.out.println(c);
        }
        
        cs.delete(delCus);
        for(Customer c: cs.getCustomers()){
            System.out.println(c);
        }
    }
}
